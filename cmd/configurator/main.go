package main

import (
	"flag"

	"fmt"
	"os"

	log "github.com/go-kit/kit/log"

	"net/http"

	"bitbucket.org/cv21/configurator"
	"bitbucket.org/cv21/configurator/middleware"
	httptransport "bitbucket.org/cv21/configurator/transport/http"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	flagDBHost     = flag.String("db.host", "localhost", "Database host")
	flagDBUser     = flag.String("db.user", "postgres", "Database user")
	flagDBName     = flag.String("db.name", "configurator", "Database name")
	flagDBPassword = flag.String("db.password", "configurator", "Database password")
	flagPort       = flag.Int("port", 9000, "Specifies port which handles service")
	flagHelp       = flag.Bool("help", false, "Shows execution parameters help")
)

func init() {
	flag.Parse()
}

func main() {
	if *flagHelp {
		flag.Usage()
		os.Exit(1)
	}

	logger := log.NewJSONLogger(os.Stdout)
	logger = log.With(logger, "@service", "configurator")
	logger = log.With(logger, "@time", log.DefaultTimestamp)
	logger = log.With(logger, "@caller", log.DefaultCaller)

	db, err := gorm.Open("postgres",
		fmt.Sprintf(
			"host=%s user=%s dbname=%s sslmode=disable password=%s",
			*flagDBHost,
			*flagDBUser,
			*flagDBName,
			*flagDBPassword,
		),
	)
	if err != nil {
		logger.Log("err", "could not connect to database", "details", err)
		os.Exit(1)
	}

	defer db.Close()

	svc := middleware.ServiceLogging(logger)(configurator.NewConfiguratorService(db))

	portConfig := fmt.Sprintf(":%d", *flagPort)
	logger.Log("listen", portConfig)

	err = http.ListenAndServe(portConfig, httptransport.NewHTTPHandler(&configurator.Endpoints{
		GetEndpoint: configurator.GetEndpoint(svc),
	}))

	logger.Log("err", err)
}
