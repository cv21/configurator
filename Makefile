dep:
	dep ensure -v

tests:
	go test ./test/... --tags=${type} -v

run:
	go run ./cmd/configurator/main.go