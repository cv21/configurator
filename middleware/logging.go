package middleware

import (
	context "context"
	time "time"

	log "github.com/go-kit/kit/log"

	configurator "bitbucket.org/cv21/configurator"
)

type Middleware func(c configurator.ConfiguratorService) configurator.ConfiguratorService

// ServiceLogging writes params, results and working time of method call to provided logger after its execution.
func ServiceLogging(logger log.Logger) Middleware {
	return func(next configurator.ConfiguratorService) configurator.ConfiguratorService {
		return &serviceLogging{
			logger: logger,
			next:   next,
		}
	}
}

type serviceLogging struct {
	logger log.Logger
	next   configurator.ConfiguratorService
}

func (L *serviceLogging) Get(ctx context.Context, t string, d string) (r interface{}, err error) {
	defer func(begin time.Time) {
		L.logger.Log(
			"method", "Get",
			"Type", t,
			"Data", d,
			"err", err,
			"took", time.Since(begin))
	}(time.Now())
	return L.next.Get(ctx, t, d)
}
