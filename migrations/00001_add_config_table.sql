-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE configs
(
  id         SERIAL NOT NULL
    CONSTRAINT configs_pkey
    PRIMARY KEY,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at TIMESTAMP WITH TIME ZONE,
  type       TEXT,
  data       TEXT,
  parameters BYTEA
);

CREATE INDEX idx_configs_deleted_at
  ON configs (deleted_at);

CREATE INDEX idx_configs_type
  ON configs (type);

CREATE INDEX idx_configs_data
  ON configs (data);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE configs;