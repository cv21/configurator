-- +goose Up
-- SQL in this section is executed when the migration is applied.
INSERT INTO public.configs (id, created_at, updated_at, deleted_at, type, data, parameters) VALUES (1, '2016-01-01 09:30:12.000000', '2016-01-01 09:30:12.000000', null, 'Develop.mr_robot', 'Database.processing', '{ "host": "localhost", "port": "5432", "database": "devdb", "user": "mr_robot", "password": "secret", "schema": "public" }');
INSERT INTO public.configs (id, created_at, updated_at, deleted_at, type, data, parameters) VALUES (2, '2016-01-01 09:30:12.000000', '2017-12-18 20:21:09.697391', null, 'Test.vpn', 'Rabbit.log', '{ "host": "10.0.5.42", "port": "5671", "virtualhost": "/", "user": "guest", "password": "guest" }');
INSERT INTO public.configs (id, created_at, updated_at, deleted_at, type, data, parameters) VALUES (3, '2016-01-01 09:30:12.000000', '2017-12-18 20:21:09.697391', '2017-12-18 20:21:09.697391', 'Some.val', 'Hel.lo', '{ "hi": "man" }');

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DELETE FROM public.configs WHERE ID IN (1, 2, 3);