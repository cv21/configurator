package repository

import (
	"github.com/jinzhu/gorm"
	"github.com/sas1024/gorm-jsonb/jsonb"
)

type Config struct {
	gorm.Model
	Type       string `gorm:"index"`
	Data       string `gorm:"index"`
	Parameters jsonb.JSONB
}
