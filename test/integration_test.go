// +build integration

package test

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/go-testfixtures/testfixtures"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"bitbucket.org/cv21/configurator/repository"
	"github.com/stretchr/testify/assert"
	"strings"
)

var (
	flagDBHost     = flag.String("db.host", "localhost", "Database host")
	flagDBUser     = flag.String("db.user", "postgres", "Database user")
	flagDBName     = flag.String("db.name", "configurator_test", "Database name")
	flagDBPassword = flag.String("db.password", "configurator", "Database password")

	db       *gorm.DB
	fixtures *testfixtures.Context
)

func init() {
	flag.Parse()
}

func TestMain(m *testing.M) {
	db, err := gorm.Open("postgres",
		fmt.Sprintf(
			"host=%s user=%s dbname=%s sslmode=disable password=%s",
			*flagDBHost,
			*flagDBUser,
			*flagDBName,
			*flagDBPassword,
		),
	)
	if err != nil {
		log.Fatalf("could not connect to database: %v", err)
	}

	err = db.AutoMigrate(repository.Config{}).Error
	if err != nil {
		log.Fatalf("could not automigrate: %v", err)
	}

	fixtures, err = testfixtures.NewFolder(db.DB(), &testfixtures.PostgreSQL{}, "./fixtures")
	if err != nil {
		log.Fatalf("could not parse fixtures: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Fatalf("could not load fixtures: %v", err)
	}

	result := m.Run()

	err = db.DropTableIfExists(repository.Config{}).Error
	if err != nil {
		log.Fatalf("could not clear fixtures: %v", err)
	}

	db.Close()

	os.Exit(result)
}

func TestGetNotFound(t *testing.T) {
	resp, err := http.Post("http://localhost:9000/get", "application/json", bytes.NewBuffer([]byte(`{"Type": "some.value", "Data": "something"}`)))
	assert.NoError(t, err, "get method is ok")

	b, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err, "body reading")
	assert.Equal(t, string(b), "record not found")
}


func TestGet(t *testing.T) {
	resp, err := http.Post("http://localhost:9000/get", "application/json", bytes.NewBuffer([]byte(`{"Type": "Develop.mr_robot", "Data": "Database.processing"}`)))
	assert.NoError(t, err, "get method is ok")

	b, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err, "body reading")
	assert.Equal(t, strings.Trim(string(b), "\n"), `{"host":"localhost","port":"5432","database":"devdb","user":"mr_robot","password":"secret","schema":"public"}`)
}

func TestGetDeleted(t *testing.T) {
	resp, err := http.Post("http://localhost:9000/get", "application/json", bytes.NewBuffer([]byte(`{"Type": "Some.val", "Data": "my.data"}`)))
	assert.NoError(t, err, "get method is ok")

	b, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err, "body reading")
	assert.Equal(t, string(b), "record not found")
}


