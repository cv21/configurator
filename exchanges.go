package configurator

type GetRequest struct {
	Type string `json:"Type"`
	Data string `json:"Data"`
}

type GetResponse struct {
	Response interface{}
}
