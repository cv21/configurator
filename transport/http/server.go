package transporthttp

import (
	http1 "net/http"

	configurator "bitbucket.org/cv21/configurator"
	http2 "bitbucket.org/cv21/configurator/transport/converter/http"
	http "github.com/go-kit/kit/transport/http"
)

func NewHTTPHandler(endpoints *configurator.Endpoints, opts ...http.ServerOption) http1.Handler {
	handler := http1.NewServeMux()
	handler.Handle("/get", http.NewServer(
		endpoints.GetEndpoint,
		http2.DecodeHTTPGetRequest,
		http2.EncodeHTTPGetResponse,
		opts...))
	return handler
}
