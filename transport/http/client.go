package transporthttp

import (
	url "net/url"
	strings "strings"

	configurator "bitbucket.org/cv21/configurator"
	http1 "bitbucket.org/cv21/configurator/transport/converter/http"
	http "github.com/go-kit/kit/transport/http"
)

func NewHTTPClient(addr string, opts ...http.ClientOption) (configurator.ConfiguratorService, error) {
	if !strings.HasPrefix(addr, "http") {
		addr = "http://" + addr
	}
	u, err := url.Parse(addr)
	if err != nil {
		return nil, err
	}
	return &configurator.Endpoints{GetEndpoint: http.NewClient(
		"POST",
		u,
		http1.EncodeHTTPGetRequest,
		http1.DecodeHTTPGetResponse,
		opts...,
	).Endpoint()}, nil
}
