package httpconv

import (
	bytes "bytes"
	context "context"
	json "encoding/json"
	ioutil "io/ioutil"
	http "net/http"

	configurator "bitbucket.org/cv21/configurator"
)

func CommonHTTPRequestEncoder(_ context.Context, r *http.Request, request interface{}) error {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(request); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func CommonHTTPResponseEncoder(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func DecodeHTTPGetRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req configurator.GetRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return &req, err
}

func DecodeHTTPGetResponse(_ context.Context, r *http.Response) (interface{}, error) {
	var resp configurator.GetResponse
	err := json.NewDecoder(r.Body).Decode(&resp)
	return resp, err
}

func EncodeHTTPGetRequest(ctx context.Context, r *http.Request, request interface{}) error {
	return CommonHTTPRequestEncoder(ctx, r, request)
}

func EncodeHTTPGetResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return CommonHTTPResponseEncoder(ctx, w, response)
}
