package configurator

import (
	context "context"

	endpoint "github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	GetEndpoint endpoint.Endpoint
}

func (E *Endpoints) Get(ctx context.Context, t string, d string) (r interface{}, err error) {
	endpointGetRequest := GetRequest{
		Data: d,
		Type: t,
	}
	endpointGetResponse, err := E.GetEndpoint(ctx, &endpointGetRequest)
	if err != nil {
		return
	}
	return endpointGetResponse.(*GetResponse).Response, err
}

func GetEndpoint(svc ConfiguratorService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_req := request.(*GetRequest)
		r, err := svc.Get(ctx, _req.Type, _req.Data)
		return r, err
	}
}
