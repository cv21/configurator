# Configurator

The project is a service which retrieving configuration from database source.

### Framework
Service uses [go-kit](gokit.io) microservice framework

### Code generation
I used a [microgen](github.com/devimteam/microgen) for [go-kit](gokit.io) boilerplate code generation.
Microgen is a small utility, which mostly designed and written by me and it is fantastically helpful for microservice code generation.

### Migrations
Use [goose](https://bitbucket.org/liamstask/goose) for run migrations

Example:
```
goose -path ./migrations status
```

### Testing

```
make tests type=integration
```

\* note: previously you need to create database `configurator_test`

### Running

Privide `-help` flag for viewing execution parameters
```
./configurator -help
```