package configurator

import (
	"context"

	"bitbucket.org/cv21/configurator/repository"
	"github.com/jinzhu/gorm"
)

type ConfiguratorService interface {
	Get(ctx context.Context, t string, d string) (r interface{}, err error)
}

type configuratorService struct {
	db *gorm.DB
}

func NewConfiguratorService(db *gorm.DB) ConfiguratorService {
	return &configuratorService{
		db: db,
	}
}

func (c *configuratorService) Get(ctx context.Context, t string, d string) (r interface{}, err error) {
	conf := &repository.Config{}
	err = c.db.Find(conf, "type = ? AND data = ?", t, d).Error
	if err != nil {
		return nil, err
	}

	return conf.Parameters, nil
}
